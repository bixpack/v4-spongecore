package de.bixilon.bixpack.v4.spongecore.main;

import org.spongepowered.api.text.Text;

public class Strings {
	public final static int COMMAND_CONSOLE_NOT_ALLOWED = 1;
	public final static int COMMAND_PING_SPONGE_SINGLE = 2;
	public final static int COMMAND_PING_SPONGE_OTHERS = 3;

	public final static int PLOT_NO_PERMISSION = 4;

	public final static int COMMAND_PLOT_USAGE = 5;
	public final static int COMMAND_PLOT_ADD_SUCCESSFUL = 6;
	public final static int COMMAND_PLOT_REMOVE_SUCCESSFUL = 7;
	public final static int COMMAND_PLOT_NOT_OWNER = 8;
	public final static int COMMAND_PLOT_ADD_ALREADY = 9;
	public final static int COMMAND_PLOT_REMOVE_NO_ACCESS = 10;
	public final static int COMMAND_PLOT_REMOVE_OWNER = 11;
	public final static int COMMAND_PLOT_ADD_TARGET = 12;
	public final static int COMMAND_PLOT_REMOVE_TARGET = 13;
	public final static int COMMAND_PLOT_ADMIN_USAGE = 14;
	public final static int COMMAND_PLOT_ADMIN_ALREADY_OWN = 15;
	public final static int COMMAND_PLOT_ADMIN_NO_PLOT = 16;
	public final static int COMMAND_PLOT_ADMIN_TELEPORT = 17;
	public final static int COMMAND_PLOT_ADMIN_POS_SAVED = 18;
	public final static int COMMAND_PLOT_ADMIN_NOT_FOUND = 19;
	public final static int COMMAND_PLOT_ADMIN_SET_OWNER = 20;
	public final static int COMMAND_PLOT_ADMIN_ADD_ALLOWED = 21;
	public final static int COMMAND_PLOT_ADMIN_REMOVE_ALLOWED = 22;
	public final static int COMMAND_PLOT_ADMIN_PLOT_INFO = 23;
	public final static int COMMAND_PLOT_ADMIN_SAVE_FAIL = 24;
	public final static int COMMAND_PLOT_ADMIN_SAVE_ERROR = 25;
	public final static int COMMAND_PLOT_ADMIN_SAVE_OK = 26;
	public final static int COMMAND_PLOT_ADMIN_POS_FAILED = 27;
	public final static int COMMAND_PLOT_ADMIN_CREATE_OK = 28;

	public final static int EVENT_GAMEMODE_NO_PERMS_TARGET = 29;
	public final static int EVENT_GAMEMODE_NO_PERMS = 30;

	public final static int COMMAND_GENERAL_PLAYER_NOT_FOUND = 100;
	public final static int COMMAND_GENERAL_NO_PERMISSIONS = 101;
	public final static int GENERAL_UNKNOWN_ERROR = 102;
	public final static int COMMAND_VERSION = 103;

	public static String getString(int name, String a[]) {
		switch (name) {
		case COMMAND_CONSOLE_NOT_ALLOWED:
			return "§cLappen";

		case COMMAND_PING_SPONGE_SINGLE:
			return "§3Du hast einen Server Ping von §e" + a[0] + "§3ms!";
		case COMMAND_PING_SPONGE_OTHERS:
			return "§e" + a[1] + " §3hat einen Server Ping von §e" + a[0] + "§3ms!";

		case COMMAND_VERSION:
			return "§3Spongecore Version: 0.07!";

		case PLOT_NO_PERMISSION:
			return "§cDas ist nicht dein Grundstück!";

		case COMMAND_PLOT_USAGE:
			return "§6Benutzung: /p <Aktion> <Spieler>\n§cMögliche Aktionen sind: add, remove!";
		case COMMAND_PLOT_ADD_SUCCESSFUL:
			return "§a" + a[0] + " hat nun Zugriff auf den Grundstück!";
		case COMMAND_PLOT_REMOVE_SUCCESSFUL:
			return "§a" + a[0] + " hat nun keinen Zugriff mehr auf den Grundstück!";
		case COMMAND_PLOT_ADD_ALREADY:
			return "§c" + a[0] + " hat bereits Rechte auf deinem Grundstück!";
		case COMMAND_PLOT_REMOVE_NO_ACCESS:
			return "§c" + a[0] + " hat keine Rechte auf deinem Grundstück!";
		case COMMAND_PLOT_NOT_OWNER:
			return "§cDu hast kein Grundstück oder bist kein Owner!";
		case COMMAND_PLOT_REMOVE_OWNER:
			return "§cDas ist dein Grundstück!";
		case COMMAND_PLOT_ADD_TARGET:
			return "§3Du hast nun Zugriff auf §e" + a[0] + "s §3Grundstück!";
		case COMMAND_PLOT_REMOVE_TARGET:
			return "§cDu hast nun keinen Zugriff mehr auf §e" + a[0] + "s §3Grundstück!";
		case COMMAND_PLOT_ADMIN_USAGE:
			return "§6Benutzung: /pa <Plot> <Aktion> <Spieler>\n§cPlot erstellen: /pa <Owner> create\n§cPosition setzten: /pa <Eigentümer> pos pos1/pos2\n§cRechte ändern: /pa <Eigentümer> <set/add/remove> <Spieler>\n§cPlot speichern /pa <Eigentümer> save!";
		case COMMAND_PLOT_ADMIN_ALREADY_OWN:
			return "§c" + a[0] + " besitzt bereits ein Grundstück!";
		case COMMAND_PLOT_ADMIN_NO_PLOT:
			return "§c" + a[0] + " besitzt kein Grundstück!";
		case COMMAND_PLOT_ADMIN_POS_SAVED:
			return "§aDie Position wurde gesetzt(" + a[0] + "§a).";
		case COMMAND_PLOT_ADMIN_TELEPORT:
			return "§aDu wurdest teleportiert!";
		case COMMAND_PLOT_ADMIN_NOT_FOUND:
			return "§aPlot Owner nicht online!";
		case COMMAND_PLOT_ADMIN_ADD_ALLOWED:
			return "§a " + a[0] + " hat nun Zugriff auf das Grundstück!";
		case COMMAND_PLOT_ADMIN_REMOVE_ALLOWED:
			return "§a " + a[0] + " hat nun keinen Zugriff mehr auf das Grundstück!";
		case COMMAND_PLOT_ADMIN_SET_OWNER:
			return "§aDer Besitzer des Plots wurde geändert!";
		case COMMAND_PLOT_ADMIN_PLOT_INFO:
			return "§c----------------Plot Info----------------\n§6Besitzer: " + a[0] + "\n§7Position 1: X:" + a[1]
					+ " Z: " + a[2] + "\n§6Position 2: X: " + a[3] + " Z:" + a[4] + "\n§7Berechtigte Spieler: " + a[5];
		case COMMAND_PLOT_ADMIN_SAVE_FAIL:
			return "§cEs gibt nichts zum Speichern!";
		case COMMAND_PLOT_ADMIN_SAVE_ERROR:
			return "§4Plot konnte nicht gespeichert werden: Sind alle Werte(Positionen,...) gesetzt?";
		case COMMAND_PLOT_ADMIN_SAVE_OK:
			return "§aPlot wurde gespeichert und angewandt";
		case COMMAND_PLOT_ADMIN_POS_FAILED:
			return "§cPosition überlappt mit anderem Plot!";
		case COMMAND_PLOT_ADMIN_CREATE_OK:
			return "§aGrundstück erstellt...";
		case EVENT_GAMEMODE_NO_PERMS_TARGET:
			return "§cFehler beim gehen in den GameMode!";
		case EVENT_GAMEMODE_NO_PERMS:
			return "§cDer Spieler ist nicht in der Teamleitung!";
			
			
		case COMMAND_GENERAL_PLAYER_NOT_FOUND:
			return "§cSpieler nicht gefunden!";
		case COMMAND_GENERAL_NO_PERMISSIONS:
			return "§cDu hast keine Rechte!";
		case GENERAL_UNKNOWN_ERROR:
			return "§4Es ist ein unbekannter Fehler aufgetreten!\n§cBitte melde das dem Team!";

		}
		return "";
	}

	public static String getString(int name) {
		return getString(name, null);
	}

	public static Text getText(int name) {
		return Text.of(getString(name, null));
	}

	public static Text getText(int name, String a[]) {
		return Text.of(getString(name, a));
	}

}
