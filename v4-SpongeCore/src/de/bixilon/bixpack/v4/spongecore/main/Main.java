package de.bixilon.bixpack.v4.spongecore.main;

import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;

// Imports for logger
import com.google.inject.Inject;

import de.bixilon.bixpack.v4.spongecore.classes.Plot;
import de.bixilon.bixpack.v4.spongecore.commands.Ping;
import de.bixilon.bixpack.v4.spongecore.commands.PlotAdmin;
import de.bixilon.bixpack.v4.spongecore.commands.PlotCommand;
import de.bixilon.bixpack.v4.spongecore.events.AntiJoinLeave;
import de.bixilon.bixpack.v4.spongecore.events.GameModeEvent;
import de.bixilon.bixpack.v4.spongecore.events.PlotEvent;
import de.bixilon.bixpack.v4.spongecore.mysql.Driver;
import de.bixilon.bixpack.v4.spongecore.mysql.MySQL;

import org.slf4j.Logger;

@Plugin(id = "v4-spongecore", name = "SpongeCore", version = "0.07", description = "Core for all Forge Servers")
public class Main {

	public static MySQL mysql;
	@Inject
	private Logger logger;

	public static Main instance;
	@Listener
	public void onServerStart(GameStartedServerEvent event) {
		logger.info("§aEnabled SpongeCore!");
	}
	public void sendLog(String message) {
		logger.info(message);
	}

	public static Main getInstance() {
		return instance;
	}
	@Listener
	public void onServerStart(GameStartingServerEvent event) {
		instance = this;
		logger.info("§3Starting SpongeCore...");
		registerCommands();
		registerEvents();
		logger.info("§7Connecting to MySQL...");
		mysql = new MySQL(new Driver(Settings.mysql_host, Settings.mysql_port, Settings.mysql_db, Settings.mysql_user,
				Settings.mysql_pw));
		if (mysql.isConnected())
			logger.info("§aMySQL connected!");
		else {
			logger.warn("§4Connection to MySQL failed!");
			System.exit(2);
		}
		fetchPlots(null);
	}

	void registerCommands() {

		CommandSpec PingCMD = CommandSpec.builder().description(Text.of("Ping Command"))
				.arguments(GenericArguments.optional(GenericArguments.string(Text.of("player")))).executor(new Ping())
				.build();
		// usage /p <action> <Player>
		CommandSpec PlotCMD = CommandSpec.builder().description(Text.of("Plot"))
				.arguments(GenericArguments.optional(GenericArguments.string(Text.of("action"))),GenericArguments.optional(GenericArguments.string(Text.of("player")))).executor(new PlotCommand())
				.build();
		// usage /pa <Plot/Player> <action> <Player>
		CommandSpec PlotAdminCMD = CommandSpec.builder().description(Text.of("Plot"))
				.arguments(GenericArguments.optional(GenericArguments.string(Text.of("plot"))),GenericArguments.optional(GenericArguments.string(Text.of("action"))),GenericArguments.optional(GenericArguments.string(Text.of("target")))).executor(new PlotAdmin())
				.build();

		Sponge.getCommandManager().register(this, PingCMD, "ping");
		Sponge.getCommandManager().register(this, PlotCMD, "plot", "p", "gs", "property");
		Sponge.getCommandManager().register(this, PlotAdminCMD, "pa");
	}

	void registerEvents() {

		Sponge.getEventManager().registerListeners(this, new AntiJoinLeave());
		Sponge.getEventManager().registerListeners(this, new GameModeEvent());
		Sponge.getEventManager().registerListeners(this, new PlotEvent());
	}

	void fetchPlots(String server) {
		// Must get dynamic!!!!
		server = "dorf";
		Plot.plots = mysql.getAllPlots(server);

	}

}