package de.bixilon.bixpack.v4.spongecore.events;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.living.humanoid.ChangeGameModeEvent;
import org.spongepowered.api.event.filter.cause.Root;

import de.bixilon.bixpack.v4.spongecore.main.Strings;

public class GameModeEvent {

    @Listener
    public void onJoin(ChangeGameModeEvent.TargetPlayer e, @Root Player p) {
    	if(e.getGameMode()  == GameModes.CREATIVE) {

        	if(!p.hasPermission("gamemode.1")) {
        		if(e.getSource() instanceof Player) {
        			Player so = (Player) e.getSource();
            		so.sendMessage(Strings.getText(Strings.EVENT_GAMEMODE_NO_PERMS));
            		p.sendMessage(Strings.getText(Strings.EVENT_GAMEMODE_NO_PERMS_TARGET));
            		e.setCancelled(true);
            		p.gameMode().set(e.getOriginalGameMode());
        		}
        	}
    	}
    }
    
    
}