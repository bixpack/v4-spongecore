package de.bixilon.bixpack.v4.spongecore.events;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import de.bixilon.bixpack.v4.spongecore.classes.Plot;
import de.bixilon.bixpack.v4.spongecore.main.Strings;

public class PlotEvent {

	@Listener
	public void onBreak(ChangeBlockEvent.Break e, @Root Player p) {
		if (!e.isCancelled()) {
			if (e.getCause().first(Player.class).isPresent()) {
				// Player broke block; checking gs
				BlockSnapshot blockSnapshot = e.getTransactions().get(0).getOriginal();
				if (!checkAllGS(p, blockSnapshot.getLocation().get())) {
					e.setCancelled(true);
					p.sendMessage(Strings.getText(Strings.PLOT_NO_PERMISSION));
				}

			}

		}
	}

	@Listener
	public void onBuild(ChangeBlockEvent.Place e, @Root Player p) {
		if (!e.isCancelled()) {
			if (e.getCause().first(Player.class).isPresent()) {
				// Player broke block; checking gs
				BlockSnapshot blockSnapshot = e.getTransactions().get(0).getOriginal();
				if (!checkAllGS(p, blockSnapshot.getLocation().get())) {
					e.setCancelled(true);
					p.sendMessage(Strings.getText(Strings.PLOT_NO_PERMISSION));
				}

			}

		}
	}

	public static boolean checkAllGS(Player p, Location<World> l) {
		for (int i = 0; i < Plot.plots.size(); i++) {
			int a = Plot.plots.get(i).allowed(p, l);

			if (a == 2) {
				// Allowed to build
				return true;
			} else if (a == 1) {
				// Disallowed to build
				return false;
			} else { // a = 0
				// no value... continue
			}

		}
		// Not on an gs. Allowing
		return true;
	}
}