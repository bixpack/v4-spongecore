package de.bixilon.bixpack.v4.spongecore.events;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;

public class AntiJoinLeave {

    @Listener
    public void onJoin(ClientConnectionEvent.Join  e) {
        e.setMessageCancelled(true);
    }
    
    @Listener
    public void onQuit(ClientConnectionEvent.Disconnect  e) {
        e.setMessageCancelled(true);
    }
}