package de.bixilon.bixpack.v4.spongecore.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.UUID;

import de.bixilon.bixpack.v4.spongecore.classes.Plot;
import de.bixilon.bixpack.v4.spongecore.main.Main;

public class MySQL {
	Driver driver;

	public MySQL(Driver d) {
		driver = d;
		AntiDisconnect.setDriver(d);
		Timer timer = new Timer();
		timer.schedule(new AntiDisconnect(), 0, 60000);
	}

	public boolean isConnected() {
		return driver.connected;
	}

	public boolean isPlayerWhitelisted(UUID uuid) {
		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery("SELECT * FROM `whitelist` WHERE `uuid` = '" + uuid + "';");

			if (rs.next())
				return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public List<Plot> getAllPlots(String server) {

		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery("SELECT * FROM `plots` WHERE `server` = '" + server + "';");
			List<Plot> ret = new ArrayList<Plot>();

			while (rs.next()) {
				String[] allowed = rs.getString("allowed").split(",");
				List<UUID> allowedUUID = new ArrayList<UUID>();
				for (int i = 0; i < allowed.length; i++) {
					if (allowed[i].length() == 36)
						allowedUUID.add(UUID.fromString(allowed[i]));
				}
				Main.getInstance()
						.sendLog("Added Plot: " + rs.getString("id") + ". Owned by: " + rs.getString("owner"));

				Plot p = new Plot(rs.getInt("id"), rs.getString("description"), rs.getInt("dimension"),
						UUID.fromString(rs.getString("owner")), allowedUUID, rs.getBoolean("blockall"), rs.getInt("x1"),
						rs.getInt("x2"), rs.getInt("z1"), rs.getInt("z2"));
				ret.add(p);
			}
			return ret;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void savePlot(Plot p) {

		Statement s = driver.getStatement();
		try {
			s.execute("UPDATE `plots` SET `description`='" + p.getDsc().replace("\"", "")
					+ "',`server`='dorf',`dimension`=" + p.getDim() + ",`owner`='" + p.getOwner().toString()
					+ "', `allowed`='" + p.getAllowedByString() + "',`blockall`= 0,`x1`=" + p.getX1() + ",`x2`="
					+ p.getX2() + ",`z1`=" + p.getZ1() + ",`z2`=" + p.getZ2() + " WHERE `id` = " + p.getID() + "");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int addPlot(Plot p) {

		Statement s = driver.getStatement();
		try {
			s.execute(
					"INSERT INTO `plots` (`description`, `server`, `dimension`, `owner`, `allowed`, `blockall`, `x1`, `x2`, `z1`, `z2`) VALUES ('-/-', 'dorf', '0', '"
							+ p.getOwner() + "', '" + p.getAllowedByString() + "', '0', '" + p.getX1() + "', '"
							+ p.getX2() + "', '" + p.getZ1() + "', '" + p.getZ2() + "');");

			ResultSet rs = s.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

}
