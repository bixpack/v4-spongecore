package de.bixilon.bixpack.v4.spongecore.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class Plot {

	public static List<Plot> plots = new ArrayList<Plot>();
	int id;
	String description;
	int dimension;

	UUID owner;

	List<UUID> allowed = new ArrayList<UUID>();
	boolean blockall = false;

	int x1;
	int x2;
	int z1;
	int z2;

	boolean okx1 = false;
	boolean okx2 = false;
	boolean okz1 = false;
	boolean okz2 = false;

	public UUID getOwner() {
		return owner;
	}

	public String getDsc() {
		return description;
	}

	public int getDim() {
		return dimension;
	}

	public int getID() {
		return id;
	}

	public List<UUID> getAllowed() {
		return allowed;
	}

	public String getAllowedByString() {
		String ret = "";
		for (UUID a : allowed) {
			ret += a.toString() + ",";
		}
		if (ret.endsWith(","))
			ret = ret.substring(0, ret.length() - 1);
		return ret;
	}

	public int getX1() {
		return x1;
	}

	public int getX2() {
		return x2;
	}

	public int getZ1() {
		return z1;
	}

	public int getZ2() {
		return z2;
	}

	public void setX1(int x) {
		okx1 = true;
		x1 = x;
	}

	public void setX2(int x) {
		okx2 = true;
		x2 = x;
	}

	public void setZ1(int z) {
		okz1 = true;
		z1 = z;
	}

	public void setZ2(int z) {
		okz2 = true;
		z2 = z;
	}

	public void setID(int id) {
		this.id = id;
	}

	public void setOwner(UUID u) {
		owner = u;
	}

	public void initXCoordinates() {
		if (isOk()) {
			if (x1 > x2) {
				int tx1 = x1;
				int tx2 = x2;
				this.x1 = tx2;
				this.x2 = tx1;
			}
		}
	}

	public void initZCoordinates() {
		if (isOk()) {
			if (z1 > z2) {
				int tz1 = z1;
				int tz2 = z2;
				this.z1 = tz2;
				this.z2 = tz1;
			}
		}
	}

	public Plot(int id, String description, int dimension, UUID owner, List<UUID> allowed, boolean blockall, int x1,
			int x2, int z1, int z2) {
		if (x1 <= x2) {
			this.x1 = x1;
			this.x2 = x2;
		} else {
			this.x1 = x2;
			this.x2 = x1;
		}

		if (z1 <= z2) {
			this.z1 = z1;
			this.z2 = z2;
		} else {
			this.z1 = z2;
			this.z2 = z1;
		}
		this.id = id;
		this.description = description;
		this.dimension = dimension;
		this.owner = owner;
		this.allowed = allowed;
		this.blockall = blockall;

	}

	public boolean isAllowed(UUID uuid) {
		if (owner.equals(uuid) || allowed.contains(uuid))
			return true;
		return false;
	}

	public int allowed(Player p, Location<World> l) {
		// l = Location of Block not player!
		int ret = 0;
		int dimID = 78978970;
		if (p.getWorld().getDimension().getType().getId().equalsIgnoreCase("minecraft:overworld"))
			dimID = 0;
		if (dimID == dimension && isInPlot(l)) {

			ret = 1;
			if (owner.equals(p.getUniqueId()) || allowed.contains(p.getUniqueId())) {
				ret = 2;
			}
		}
		return ret;

	}

	public void addAllowed(UUID uuid) {
		allowed.add(uuid);
	}

	public void removeAllowed(UUID uuid) {
		allowed.remove(uuid);
	}

	public boolean isInPlot(Location<World> l) {

		int lx = l.getBlockX();
		int lz = l.getBlockZ();

		boolean wasinx = false;
		boolean wasinz = false;

		if (lx > x1 && lx < x2)
			wasinx = true;
		if (lx == x1 || lx == x2)
			wasinx = true;

		if (lz > z1 && lz < z2)
			wasinz = true;
		if (lz == z1 || lz == z2)
			wasinz = true;

		if (wasinx && wasinz)
			return true;

		return false;
	}

	public static int getPlotIDbyOwner(UUID uuid) {
		for (int i = 0; i < plots.size(); i++) {
			if (plots.get(i).getOwner().equals(uuid))
				return i;
		}
		return -1;
	}

	public boolean isOk() {
		// true when all values are set
		if (okx1 && okx2 && okz1 && okz2 && owner != null)
			return true;
		return false;
	}
}
