package de.bixilon.bixpack.v4.spongecore.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import de.bixilon.bixpack.v4.spongecore.classes.Plot;
import de.bixilon.bixpack.v4.spongecore.main.Main;
import de.bixilon.bixpack.v4.spongecore.main.Strings;

public class PlotAdmin implements CommandExecutor {
	public HashMap<UUID, Plot> progress = new HashMap<UUID, Plot>();

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (src.hasPermission("plot.admin")) {
			if (args.<String>getOne("action").isPresent()) {
			    
				if (Sponge.getServer().getPlayer(args.<String>getOne("plot").get()) == null) {
					src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_NOT_FOUND));
					return CommandResult.success();
				}
				Player plotowner = Sponge.getServer().getPlayer(args.<String>getOne("plot").get()).get();

				String action = args.<String>getOne("action").get().toLowerCase();
				if (action.equalsIgnoreCase("create")) {
					// Create plot
					if (Plot.getPlotIDbyOwner(plotowner.getUniqueId()) != -1) {
						src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_ALREADY_OWN,
								new String[] { plotowner.getName() }));
						return CommandResult.success();
					}
					progress.put(plotowner.getUniqueId(),
							new Plot(0, "", 0, plotowner.getUniqueId(), new ArrayList<UUID>(), false, 0, 0, 0, 0));
					// int id, String description, int dimension, UUID owner, List<UUID> allowed,
					// boolean blockall, int x1,
					// int x2, int z1, int z2
					src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_CREATE_OK));

				} else {
					int list = 0; // 1= plots list, 2 = progress list; 3/0 = not found
					Plot p;
					int plotid = Plot.getPlotIDbyOwner(plotowner.getUniqueId());
					if (plotid == -1) {
						// Must be in progress list or null
						if (progress.containsKey(plotowner.getUniqueId())) {
							p = progress.get(plotowner.getUniqueId());
							list = 2;
						} else {
							// Player does not have a plot
							src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_NO_PLOT,
									new String[] { plotowner.getName() }));
							return CommandResult.success();
						}
					} else {
						list = 1;
						p = Plot.plots.get(plotid);
					}
					if (action.equalsIgnoreCase("pos") || action.equalsIgnoreCase("tp")) {
						// Set position of plot
						if (src instanceof Player) {
							Player admin = (Player) src;
							String target = args.<String>getOne("target").get();
							boolean pos; // false = 2; true = 1;
							if (target.equalsIgnoreCase("pos1"))
								pos = true;
							else if (target.equalsIgnoreCase("pos2"))
								pos = false;
							else {
								src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_USAGE));
								return CommandResult.success();
							}

							if (action.equalsIgnoreCase("tp")) {
								Location<World> loc;
								if (pos)
									loc = new Location<World>(Sponge.getServer().getWorld("world").get(), p.getX1(),
											admin.getWorld().getHighestYAt(p.getX1(), p.getZ1()), p.getZ1());
								else
									loc = new Location<World>(Sponge.getServer().getWorld("world").get(), p.getX2(),
											admin.getWorld().getHighestYAt(p.getX2(), p.getZ2()), p.getZ2());
								admin.setLocation(loc);

								src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_TELEPORT));
								return CommandResult.success();
							} else {
								// set position
								Location<World> loc = admin.getLocation();
								if (isInPlot(loc)) {
									src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_POS_FAILED));
									return CommandResult.success();
								}
								if (pos) {
									// 1
									p.setX1(loc.getBlockX());
									p.setZ1(loc.getBlockZ());
									p.initXCoordinates();
								} else {
									p.setX2(loc.getBlockX());
									p.setZ2(loc.getBlockZ());
									p.initZCoordinates();
								}
								src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_POS_SAVED, new String[] {"§3X: §e" + loc.getBlockX() + " §3Z: §e" + loc.getBlockZ()}));
							}

						} else {
							src.sendMessage(Strings.getText(Strings.COMMAND_CONSOLE_NOT_ALLOWED));
						}
					} else if (action.equalsIgnoreCase("set") || action.equalsIgnoreCase("add")
							|| action.equalsIgnoreCase("remove")) {
						String target = args.<String>getOne("target").get();
						Player ptarget = Sponge.getServer().getPlayer(target).get();
						if (ptarget == null) {
							src.sendMessage(Strings.getText(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
							return CommandResult.success();
						}
						if (action.equalsIgnoreCase("set")) {
							p.setOwner(ptarget.getUniqueId());
							src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_SET_OWNER,
									new String[] { ptarget.getName() }));
						} else if (action.equalsIgnoreCase("add")) {
							p.addAllowed(ptarget.getUniqueId());
							src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_ADD_ALLOWED,
									new String[] { ptarget.getName() }));
						} else {
							// remove
							p.removeAllowed(ptarget.getUniqueId());
							src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_REMOVE_ALLOWED));
						}

					} else if (action.equalsIgnoreCase("save") || action.equalsIgnoreCase("info")) {
						if (action.equalsIgnoreCase("save")) {
							if (list == 2) {
								if (p.isOk()) {
									progress.remove(plotowner.getUniqueId());
									int id = Main.mysql.addPlot(p);
									p.setID(id);
									Plot.plots.add(p);
									src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_SAVE_OK));
								} else {
									src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_SAVE_ERROR));
									return CommandResult.success();
								}

							} else {
								src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_SAVE_FAIL));
								return CommandResult.success();
							}

						} else {
							// info
							String allowedS = "";
							List<UUID> allowed = p.getAllowed();
							for (int i = 0; i < allowed.size(); i++) {
								String t;
								if (Sponge.getServer().getPlayer(allowed.get(i)).get() != null)
									t = Sponge.getServer().getPlayer(allowed.get(i)).get().getName();
								else
									t = allowed.get(i).toString().substring(0, 3) + "?";
								allowedS += t + ",";
							}
							if (allowedS.endsWith(","))
								allowedS = allowedS.substring(0, allowedS.length() - 1);

							src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_PLOT_INFO,
									new String[] { plotowner.getName(), String.valueOf(p.getX1()),
											String.valueOf(p.getZ1()), String.valueOf(p.getX2()),
											String.valueOf(p.getZ2()), allowedS }));
							return CommandResult.success();
						}
						// save plot
						if (list == 1) {

							Plot.plots.remove(plotid);
							Plot.plots.add(plotid, p);
						}else if(list == 2) {
							progress.remove(plotowner.getUniqueId());
							progress.put(plotowner.getUniqueId(), p);
						}

					} else {
						src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_USAGE));
					}
				}

			} else {
				src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADMIN_USAGE));
			}
		} else {
			src.sendMessage(Strings.getText(Strings.COMMAND_GENERAL_NO_PERMISSIONS));
		}

		return CommandResult.success();
	}

	public static boolean isInPlot(Location<World> loc) {
		boolean ret = false;
		for (int i = 0; i < Plot.plots.size(); i++) {
			ret = Plot.plots.get(i).isInPlot(loc);
			if (ret)
				return ret;

		}
		return ret;
	}
}
