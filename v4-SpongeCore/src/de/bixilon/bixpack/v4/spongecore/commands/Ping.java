package de.bixilon.bixpack.v4.spongecore.commands;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;

import de.bixilon.bixpack.v4.spongecore.main.Strings;

public class Ping implements CommandExecutor {

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (!args.<String>getOne("player").isPresent()) {
			if (src instanceof Player) {
				Player p = (Player) src;
				p.sendMessage(Strings.getText(Strings.COMMAND_PING_SPONGE_SINGLE,
						new String[] { String.valueOf(p.getConnection().getLatency()) }));
			} else
				src.sendMessage(Strings.getText(Strings.COMMAND_CONSOLE_NOT_ALLOWED));

		} else {
			String arg = args.<String>getOne("player").get();
			Player p = Sponge.getServer().getPlayer(arg).get();
			if (p == null)
				src.sendMessage(Strings.getText(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
			else {
				src.sendMessage(Strings.getText(Strings.COMMAND_PING_SPONGE_OTHERS, new String[] {String.valueOf(p.getConnection().getLatency()), p.getName()}));
			}
		}

		return CommandResult.success();
	}
}
