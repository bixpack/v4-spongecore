package de.bixilon.bixpack.v4.spongecore.commands;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;

import de.bixilon.bixpack.v4.spongecore.classes.Plot;
import de.bixilon.bixpack.v4.spongecore.main.Main;
import de.bixilon.bixpack.v4.spongecore.main.Strings;

public class PlotCommand implements CommandExecutor {

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (src instanceof Player) {

			if (!args.<String>getOne("player").isPresent()) {
				src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_USAGE));
				return CommandResult.success();
			}
			String action = args.<String>getOne("action").get();

			Player owner = (Player) src;
			Player target = Sponge.getServer().getPlayer(args.<String>getOne("player").get()).get();
			int plotid = Plot.getPlotIDbyOwner(owner.getUniqueId());
			if (plotid == -1) {
				src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_NOT_OWNER));
				return CommandResult.success();
			}
			if (target == null) {
				src.sendMessage(Strings.getText(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
				return CommandResult.success();
			}
			Plot plot = Plot.plots.get(plotid);
			if (action.equalsIgnoreCase("add")) {
				// Add Player to Plot
				if (plot.isAllowed(target.getUniqueId())) {
					src.sendMessage(
							Strings.getText(Strings.COMMAND_PLOT_ADD_ALREADY, new String[] { target.getName() }));

					return CommandResult.success();
				}
				plot.addAllowed(target.getUniqueId());
				src.sendMessage(
						Strings.getText(Strings.COMMAND_PLOT_ADD_SUCCESSFUL, new String[] { target.getName() }));
				target.sendMessage(Strings.getText(Strings.COMMAND_PLOT_ADD_TARGET, new String[] { src.getName() }));
			} else if (action.equalsIgnoreCase("remove")) {
				if (target == owner) {
					// Cant remove himself from owner ship(wouldn't be saved!)

					src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_REMOVE_OWNER));
					return CommandResult.success();
				}
				// Revoke Players Permission
				if (!plot.isAllowed(target.getUniqueId())) {
					src.sendMessage(
							Strings.getText(Strings.COMMAND_PLOT_REMOVE_NO_ACCESS, new String[] { target.getName() }));
					return CommandResult.success();
				}
				plot.removeAllowed(target.getUniqueId());
				src.sendMessage(
						Strings.getText(Strings.COMMAND_PLOT_REMOVE_SUCCESSFUL, new String[] { target.getName() }));
				target.sendMessage(Strings.getText(Strings.COMMAND_PLOT_REMOVE_TARGET, new String[] { src.getName() }));
			} else
				src.sendMessage(Strings.getText(Strings.COMMAND_PLOT_USAGE));
			// save plot
			Plot.plots.remove(plotid);
			Plot.plots.add(plot);
			// Save to MySQL
			Main.mysql.savePlot(plot);
		} else {
			src.sendMessage(Strings.getText(Strings.COMMAND_CONSOLE_NOT_ALLOWED));
		}

		return CommandResult.success();
	}
}
